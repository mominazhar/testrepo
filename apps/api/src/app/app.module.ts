import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Student } from './models/student';
import { GraphQLModule } from '@nestjs/graphql/dist';
import { StudentResolvers } from './resolvers/student/student.resolver';
import { StudentService } from './services/student.service';


@Module({

  imports: [
    TypeOrmModule.forRoot({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'root',
    database: 'test',
    entities: [Student],
    synchronize: true,
    }),
    
    GraphQLModule.forRoot({
      installSubscriptionHandlers: true,
      autoSchemaFile: 'schema.gql',
      context: ({ req }) => ({ headers: req.headers }),
      cors: {
        origin: "http://localhost:4200",
        credentials: true
      }
    }),
    TypeOrmModule.forFeature([Student])

],
  controllers: [AppController],
  providers: [AppService,StudentService,StudentResolvers],

})
export class AppModule {}
