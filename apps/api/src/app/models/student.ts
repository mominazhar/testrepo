import { Field, ID, Int, ObjectType } from "@nestjs/graphql";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";


@ObjectType()
@Entity()
export class Student {

	@Field(type => String)
	@PrimaryGeneratedColumn("uuid")
	id: string;

	@Field(type => String, { nullable: true })
	@Column({ type: "varchar", length: 255, nullable: true })
	name: string;

	

	@Field(type => ID, { nullable: true })
	@Column({ type: "int", nullable: true })
	city_id: number;

	
}
