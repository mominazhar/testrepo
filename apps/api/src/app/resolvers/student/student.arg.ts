import { ArgsType, Field, ID, Int } from '@nestjs/graphql';

@ArgsType()
export class StudentArgs {

	@Field(type => String, { nullable: true })
	id: String;

	@Field(type => String, { nullable: true })

	name: string;

	@Field(type => ID, { nullable: true })

	city_id: number;
}
