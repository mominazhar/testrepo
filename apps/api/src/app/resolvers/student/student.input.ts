import { Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class StudentInput {


	@Field(type => ID, { nullable: true })
	id;

	@Field(type => String, { nullable: true })
	name: string;


	@Field(type => ID, { nullable: true })
	city_id: number;


}
