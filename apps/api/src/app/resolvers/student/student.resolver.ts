import { Args, Mutation, Query, Resolver } from "@nestjs/graphql/dist";
import { Student } from "../../models/student";
import { StudentService } from "../../services/student.service";
import { StudentArgs } from "./student.arg";
import { StudentInput } from "./student.input";


@Resolver(of => Student)
export class StudentResolvers {
	constructor(private studentService: StudentService) { }


	@Query(returns => [Student])
	async getstudent(@Args() params: StudentArgs = null) {

		return await this.studentService.find({ where: params });
	}


	// @ResolveField()
	// async city(@Parent() student: student) {

	// 	const { city_id } = student;

	// 	const city = await this.cityService.find({ id: city_id });

	// 	return city[0];
	// }



	@Mutation(returns => Student)
	async createstudent(@Args("Student") student: StudentInput) {

		return await this.studentService.create(student);
	}

	@Mutation(returns => Student)
	async deletestudent(@Args("Student") student: StudentInput) {

		return await this.studentService.delete(student);
	}

	@Mutation(returns => Student)
	async updatestudent(@Args("Student") student: StudentInput, @Args("id") id: number) {

		return await this.studentService.update(id, student);

	}
}
