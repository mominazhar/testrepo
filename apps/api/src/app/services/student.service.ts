import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';
import { Student } from '../models/student';




@Injectable()
export class StudentService {
	//private readonly student: student[] = [{ id: 3, name: 'testing' }];
	constructor(
		@InjectRepository(Student)
		private readonly studentRepository: Repository<Student>,
	) { }



	async find(params = null): Promise<Student[]> {

		let result;

		if (params?.where?.name) {

			result = await this.studentRepository.find({
				where: {
					name: Like("%" + params.where.name + "%"),
				}

			});

		}
		else {
			result = await this.studentRepository.find(params);
		}


		return result;
	}



	async create(student) {

		try {

			const result = await this.studentRepository.create(student);
			await this.studentRepository.save(result);
			return result;
		}
		catch (error) {

			return null;
		}
	}
	async update(id, student) {

		try {

			const student_obj = JSON.parse(JSON.stringify(student));
			const result = await this.studentRepository.update({ id: id }, student_obj);


			if (result?.affected) {

				const student_found = await this.studentRepository.findOne({ id: id });
				if (student_found) {
					return student_found;
				}
				return null;
			}
			return null;
		} catch (error) {
			console.log("Error in Updatestudent Service", error)

		}


	}



	async delete(id) {
		try {
			const result = await this.studentRepository.delete(id);

			return result;
		}
		catch (error) {
			console.log(error)
			return null;
		}
	}
}
